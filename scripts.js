let isSubscriptionMonthly = true;
// Setting default subscription type to monthly.
// Set this variable to false to set default subscription type to yearly.
let subscriptionNotation = isSubscriptionMonthly ? "mo" : "yr";
let subscriptionType = isSubscriptionMonthly ? "Monthly" : "Yearly";
let selectedPlan;
let selectedAddOns;
let monthlyPrice = {
  arcade: 9,
  advanced: 12,
  pro: 15,
  "online-service": 1,
  "larger-storage": 2,
  "customizable-profile": 2,
};
let yearlyPrice = {
  arcade: 90,
  advanced: 120,
  pro: 150,
  "online-service": 10,
  "larger-storage": 20,
  "customizable-profile": 20,
};
let priceData = isSubscriptionMonthly ? monthlyPrice : yearlyPrice;
let idNameMap = {
  arcade: "Arcade",
  advanced: "Advanced",
  pro: "Pro",
  "online-service": "Online service",
  "larger-storage": "Larger storage",
  "customizable-profile": "Customizable profile",
};
if (!isSubscriptionMonthly) {
  document.querySelector(".slider").classList.toggle("slider-on-right");
  document.querySelectorAll(".offer").forEach((element) => {
    element.classList.toggle("hidden");
  });
}
writePriceDetailsToElements();

function navigateTo(nextstep, currentStep) {
  document.querySelector(`.step-${currentStep}`).classList.add("hidden");
  document.querySelector(`.step-${nextstep}`).classList.remove("hidden");
  if (nextstep !== 5) {
    document
      .querySelector(`.circle-${currentStep}`)
      .classList.remove("active-circle");
    document
      .querySelector(`.circle-${nextstep}`)
      .classList.add("active-circle");
  }
}

// This function removes horizontal line and empty div if no add-ons are selected.
function formatFinishingUpPage() {
  if (selectedAddOns.length === 0) {
    document.querySelector("#summary-hr").classList.add("hidden");
    document
      .querySelector(".add-ons-selections-container")
      .classList.add("hidden");
  } else {
    document.querySelector("#summary-hr").classList.remove("hidden");
    document
      .querySelector(".add-ons-selections-container")
      .classList.remove("hidden");
  }
}

function writeSelectedPlanToSummary() {
  selectedPlan = document.querySelector('input[name="plan"]:checked');
  document.querySelector("#selected-plan").innerText = `${
    idNameMap[selectedPlan.value]
  } (${subscriptionType})`;
  document.querySelector("#selected-plan-price").innerText = `$${
    priceData[selectedPlan.value]
  }/${subscriptionNotation}`;
}

function writeSelectedAddOnsToSummary() {
  let checkedBoxes = document.querySelectorAll('input[name="add-ons"]:checked');
  checkedBoxes.forEach((checkedBox) => {
    selectedAddOns.push(checkedBox.value);
    let newDiv = document.createElement("div");
    newDiv.classList.add("summary-item-add-ons");
    newDiv.innerHTML = `<div class="summary-text">${
      idNameMap[checkedBox.value]
    }</div>
    <div class="small-font">+$${
      priceData[checkedBox.value]
    }/${subscriptionNotation}</div>`;
    document.querySelector(".add-ons-selections-container").appendChild(newDiv);
  });
}

function writeTotalPriceToSummary() {
  totalAddOnsPrice = selectedAddOns.reduce((acc, addOn) => {
    return acc + priceData[addOn];
  }, 0);
  let planPrice = priceData[selectedPlan.value];
  let totalPrice = totalAddOnsPrice + planPrice;
  let subscriptionDuration = subscriptionType.slice(0, -2).toLowerCase();
  document.querySelector(
    "#total-price-text"
  ).innerText = `Total (per ${subscriptionDuration})`;
  document.querySelector(
    "#total-price"
  ).innerText = `$${totalPrice}/${subscriptionNotation}`;
}

function validate() {
  let isValidated = {
    name: false,
    email: false,
    "phone-number": false,
  };
  let idAlertMap = {
    name: "alert-message-1",
    email: "alert-message-2",
    "phone-number": "alert-message-3",
  };
  Object.keys(idAlertMap).forEach((key) => {
    if (!document.querySelector(`#${key}`).value.trim()) {
      document.querySelector(`.${idAlertMap[key]}`).classList.remove("hidden");
      document.querySelector(`[name="${key}"]`).style.borderColor =
        "hsl(354, 84%, 57%)";
      isValidated[key] = false;
    } else {
      document.querySelector(`.${idAlertMap[key]}`).classList.add("hidden");  
      document.querySelector(`[name="${key}"]`).style.borderColor =
        "hsl(229, 24%, 87%)";
      isValidated[key] = true;
    }
    const email = document.querySelector('[name="email"]').value;
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!regex.test(email) && email !== '') {
      document.querySelector(`.email-alert`).classList.remove("hidden");
      document.querySelector(`[name='email']`).style.borderColor =
        "hsl(354, 84%, 57%)";
      isValidated[key] = false;
    } else {
      document.querySelector(`.email-alert`).classList.add("hidden");
    }
  });
  let allTrue = Object.values(isValidated).every((value) => value === true);
  if (allTrue) navigateTo(2, 1);
}

function nextStepFunctionalities(currentStep) {
  switch (currentStep) {
    case 1:
      validate();
      break;
    case 2:
      writeSelectedPlanToSummary();
      break;
    case 3:
      document.querySelectorAll(".summary-item-add-ons").forEach((item) => {
        item.remove();
      });
      selectedAddOns = [];
      writeSelectedAddOnsToSummary();
      formatFinishingUpPage();
      writeTotalPriceToSummary();
  }
}

function writePriceDetailsToElements() {
  Object.keys(priceData).forEach((key, index) => {
    if (index < 3) {
      document.querySelector(
        `#${key}-price`
      ).innerText = `$${priceData[key]}/${subscriptionNotation}`;
    } else {
      document.querySelector(
        `#${key}-price`
      ).innerText = `+$${priceData[key]}/${subscriptionNotation}`;
    }
  });
}

function toggleButtonFunctionalities() {
  document.querySelector(".slider").classList.toggle("slider-on-right");
  if (isSubscriptionMonthly) isSubscriptionMonthly = false;
  else isSubscriptionMonthly = true;
  document.querySelectorAll(".offer").forEach((element) => {
    element.classList.toggle("hidden");
  });
  document.querySelectorAll(".duration").forEach((item) => {
    item.classList.toggle("duration-active");
  });
  priceData = isSubscriptionMonthly ? monthlyPrice : yearlyPrice;
  subscriptionNotation = isSubscriptionMonthly ? "mo" : "yr";
  subscriptionType = isSubscriptionMonthly ? "Monthly" : "Yearly";
  writePriceDetailsToElements();
}

function clickFunctionalities(e) {
  console.log(e.target.classList); 
  let buttonClicked = e.target.id.slice(0, -2);
  let currentStep = Number(e.target.id[e.target.id.length - 1]);
  if (buttonClicked === "next-button") {
    if (currentStep !== 1) {
      navigateTo(currentStep + 1, currentStep);
    }
    nextStepFunctionalities(currentStep);
  }
  if (buttonClicked === "back-button") {
    navigateTo(currentStep - 1, currentStep);
  }
  if (
    e.target.classList.contains("toggle-button") ||
    e.target.classList.contains("slider")
  ) {
    toggleButtonFunctionalities();
  }
  if (e.target.classList.contains("change-plan-button")) {
    navigateTo(2, 4);
  }
}

document
  .querySelector(".container")
  .addEventListener("click", clickFunctionalities);
